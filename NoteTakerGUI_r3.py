# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'NoteTakerGUI_r3.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ServerStatusWindow(object):
    def setupUi(self, ServerStatusWindow):
        ServerStatusWindow.setObjectName("ServerStatusWindow")
        ServerStatusWindow.resize(702, 292)
        ServerStatusWindow.setMinimumSize(QtCore.QSize(702, 292))
        ServerStatusWindow.setMaximumSize(QtCore.QSize(702, 292))
        self.status_display = QtWidgets.QListWidget(ServerStatusWindow)
        self.status_display.setGeometry(QtCore.QRect(10, 10, 681, 271))
        self.status_display.setMinimumSize(QtCore.QSize(681, 271))
        self.status_display.setMaximumSize(QtCore.QSize(681, 271))
        self.status_display.setObjectName("status_display")

        self.retranslateUi(ServerStatusWindow)
        QtCore.QMetaObject.connectSlotsByName(ServerStatusWindow)

    def retranslateUi(self, ServerStatusWindow):
        _translate = QtCore.QCoreApplication.translate
        ServerStatusWindow.setWindowTitle(_translate("ServerStatusWindow", "Dialog"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ServerStatusWindow = QtWidgets.QDialog()
    ui = Ui_ServerStatusWindow()
    ui.setupUi(ServerStatusWindow)
    ServerStatusWindow.show()
    sys.exit(app.exec_())

