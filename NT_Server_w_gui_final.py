import tkinter as tk
import tkinter.scrolledtext as tkst
import socket
import threading
import socketserver as SS
from time import strftime
import sys
from tkinter import messagebox

def save2file():
    with open(save_file, 'w') as f:
        textlines = editArea.get('1.0', 'end')
        f.write(textlines)
        
def closeAll():
    save2file()
    if messagebox.askokcancel("Quit", "Exiting will stop the server.  You sure you want to do that?"):
        server.shutdown()
        win.destroy()
        sys.exit()

class ThreadedTCPRequestHandler(SS.BaseRequestHandler):

    def handle(self):
        message = strftime("%j:%H:%M:%S")  + '  ' + str(self.request.recv(1024), 'ascii')
        editArea.insert(tk.INSERT, message + '\n')
        save2file()
        cur_thread = threading.current_thread()
        response = bytes(message, 'ascii')
        self.request.sendall(response)

class ThreadedTCPServer(SS.ThreadingMixIn, SS.TCPServer):
    pass

if __name__ == "__main__":
    host = '192.168.1.12'
    port = 10
    save_file = 'C:\\Users\\Lonestar\\Py300B\\Project\\Test Notes\\temp_notes.txt'

    win = tk.Tk()
    win.title('Server Status')
    frame1 = tk.Frame(
    master = win,
    
    )
    frame1.pack(fill='both', expand='yes')
    editArea = tkst.ScrolledText(
    master = frame1,
    wrap   = tk.WORD,
    )


    
    editArea.pack(padx=10, pady=10, fill=tk.BOTH, expand=True)
    
    server = ThreadedTCPServer((host, port), ThreadedTCPRequestHandler)
    ip, port = server.server_address
    server_thread = threading.Thread(target = server.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    message = strftime("%j:%H:%M:%S") + '  ' + ip + ' is waiting for connections!\n'

    editArea.insert(tk.INSERT, message)
    
    win.protocol('WM_DELETE_WINDOW', closeAll)
    win.mainloop()
    

    
    server.serve_forever()

