# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'NoteTakerGUI_r4.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ServerStatusWindow(object):
    def setupUi(self, ServerStatusWindow):
        ServerStatusWindow.setObjectName("ServerStatusWindow")
        ServerStatusWindow.resize(697, 431)
        self.connect_b = QtWidgets.QPushButton(ServerStatusWindow)
        self.connect_b.setGeometry(QtCore.QRect(50, 380, 75, 23))
        self.connect_b.setObjectName("connect_b")
        self.status_display = QtWidgets.QListWidget(ServerStatusWindow)
        self.status_display.setGeometry(QtCore.QRect(10, 10, 681, 361))
        self.status_display.setObjectName("status_display")

        self.retranslateUi(ServerStatusWindow)
        QtCore.QMetaObject.connectSlotsByName(ServerStatusWindow)

    def retranslateUi(self, ServerStatusWindow):
        _translate = QtCore.QCoreApplication.translate
        ServerStatusWindow.setWindowTitle(_translate("ServerStatusWindow", "Dialog"))
        self.connect_b.setText(_translate("ServerStatusWindow", "Connect"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ServerStatusWindow = QtWidgets.QDialog()
    ui = Ui_ServerStatusWindow()
    ui.setupUi(ServerStatusWindow)
    ServerStatusWindow.show()
    sys.exit(app.exec_())

