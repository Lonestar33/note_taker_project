import socket
import sys
import getpass

def client(ip, port, message):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((ip, port))
        try:
            sock.sendall(bytes(message, 'ascii'))
            response = str(sock.recv(1024), 'ascii')
            print("Received: {}".format(response))
        finally:
            pass

def user_connected(ip, port, username):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((ip, port))
        try:
            message = (username + ' connected!')
            sock.sendall(bytes(message, 'ascii'))
        finally:
            return message

def user_disconnected(ip, port, username):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((ip, port))
        try:
            message = (username + ' has disconnected!')
            sock.sendall(bytes(message, 'ascii'))
        finally:
            sock.close()

if __name__ == "__main__":
    ip = '127.0.0.1'
    port = 10000
    username = getpass.getuser()

    message = user_connected(ip, port, username)

    while True:
        message = input('type something: ')
        if message.lower() != 'exit':
            client(ip, port, message)
        else:
            message = user_disconnected(ip, port, username)
            break
