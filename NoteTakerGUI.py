# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'NoteTakerGUI.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ServerStatus_win(object):
    def setupUi(self, ServerStatus_win):
        ServerStatus_win.setObjectName("ServerStatus_win")
        ServerStatus_win.resize(711, 215)
        ServerStatus_win.setMinimumSize(QtCore.QSize(711, 215))
        ServerStatus_win.setMaximumSize(QtCore.QSize(711, 215))
        ServerStatus_win.setAnimated(True)
        self.centralwidget = QtWidgets.QWidget(ServerStatus_win)
        self.centralwidget.setObjectName("centralwidget")
        self.textBrowser = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser.setGeometry(QtCore.QRect(10, 10, 691, 161))
        self.textBrowser.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.textBrowser.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.textBrowser.setObjectName("textBrowser")
        ServerStatus_win.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(ServerStatus_win)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 711, 21))
        self.menubar.setObjectName("menubar")
        ServerStatus_win.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(ServerStatus_win)
        self.statusbar.setSizeGripEnabled(False)
        self.statusbar.setObjectName("statusbar")
        ServerStatus_win.setStatusBar(self.statusbar)

        self.retranslateUi(ServerStatus_win)
        QtCore.QMetaObject.connectSlotsByName(ServerStatus_win)

    def retranslateUi(self, ServerStatus_win):
        _translate = QtCore.QCoreApplication.translate
        ServerStatus_win.setWindowTitle(_translate("ServerStatus_win", "Note Taker Server Status"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ServerStatus_win = QtWidgets.QMainWindow()
    ui = Ui_ServerStatus_win()
    ui.setupUi(ServerStatus_win)
    ServerStatus_win.show()
    sys.exit(app.exec_())

