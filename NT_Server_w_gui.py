import socket
import threading
import socketserver as SS
import sys
from time import strftime
from PyQt5 import QtCore, QtGui, QtWidgets
from NoteTakerGUI import Ui_ServerStatus_win

class MyApp(Ui_ServerStatus_win):
    def __init__(self):
        super(MyApp, self).__init__()
        self.setupUi(self)
        self.show()
        
        
def text2statusbox(message):
    textBrowser.setText(message)
    
class ThreadedTCPRequestHandler(SS.BaseRequestHandler):

    def handle(self):
        message = str(self.request.recv(1024), 'ascii')
        print(message)
        MyApp.text2statusbox(message)
        cur_thread = threading.current_thread()
        response = bytes("{}: {}".format(cur_thread.name, message), 'ascii')
        self.request.sendall(response)
        

class ThreadedTCPServer(SS.ThreadingMixIn, SS.TCPServer):
    pass

if __name__ == "__main__":
    host = 'localhost'
    port = 10000

    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    
    
    print('here')
    server = ThreadedTCPServer((host, port), ThreadedTCPRequestHandler)
    ip, port = server.server_address
    server_thread = threading.Thread(target = server.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    print(strftime("%j:%H:%M:%S") + ' ' + ip + ' is waiting for connections!')
    message = strftime("%j:%H:%M:%S") + ' ' + ip + ' is waiting for connections!'
    text2statusbox(message)
    app.exec_()
    server.serve_forever()

