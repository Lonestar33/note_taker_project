import socket
import threading
import socketserver as SS
import sys
from time import strftime
from PyQt5 import QtCore, QtGui, QtWidgets
from NoteTakerGUI_r4 import Ui_ServerStatusWindow



class MyApp(Ui_ServerStatusWindow):
    def __init__(self, dialog):
        Ui_ServerStatusWindow.__init__(self)
        self.setupUi(dialog)
        
        
        #self.connect_b.clicked.connect(self.text2statusbox)
        
    def text2statusbox(self, host, port):
        host = 'localhost'
        port = 10000
        server = ThreadedTCPServer((host, port), ThreadedTCPRequestHandler)
        ip, port = server.server_address
        server_thread = threading.Thread(target = server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        print(strftime("%j:%H:%M:%S") + ' ' + ip + ' is waiting for connections!')
        message = strftime("%j:%H:%M:%S") + ' ' + ip + ' is waiting for connections!'
        self.status_display.addItem(message)
        server.serve_forever()
        
class ThreadedTCPRequestHandler(SS.BaseRequestHandler):
    def handle(self):
        message = str(self.request.recv(1024), 'ascii')
        print(message)
        text2statusbox(message)
        cur_thread = threading.current_thread()
        response = bytes("{}: {}".format(cur_thread.name, message), 'ascii')
        self.request.sendall(response)
        

class ThreadedTCPServer(SS.ThreadingMixIn, SS.TCPServer):
    pass

if __name__ == "__main__":
    host = 'localhost'
    port = 10000

    app = QtWidgets.QApplication(sys.argv)
    dialog = QtWidgets.QDialog()
    
    window = MyApp(dialog)

    dialog.show()
    #sys.exit(app.exec_())

    #setupServer(host, port, window, app)
    app.exec_()
    #sys.exit(app.exec_())
    
    
    
