import socket
import sys
import getpass

def client(ip, port, message):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((ip, port))
        try:
            sock.sendall(bytes((username + ' ' + message), 'ascii'))
            response = str(sock.recv(1024), 'ascii')
            print(response)
        except:
            print('Looks like someone stopped the server')
        finally:
            pass

def user_connected(ip, port, username):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        try:
            sock.connect((ip, port))
        except:
            print('Could not connect to ' + ip)
            sock.close()
            pass
            
        try:
            message = (username + ' connected!')
            sock.sendall(bytes(message, 'ascii'))
            print('You connected to ' + ip)
        finally:
            return message

def user_disconnected(ip, port, username):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((ip, port))
        try:
            message = (username + ' has disconnected!')
            sock.sendall(bytes(message, 'ascii'))
        finally:
            sock.close()

if __name__ == "__main__":
    ip = '192.168.1.12'
    port = 10
    username = getpass.getuser()

    message = user_connected(ip, port, username)

    while True:
        message = input()
        if message.lower() != 'exit':
            client(ip, port, message)
        else:
            message = user_disconnected(ip, port, username)
            break
